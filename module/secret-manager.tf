resource "aws_secretsmanager_secret" "default" {
  count = var.use_password ? 1 : 0

  name = "Secret-${var.cluster_name}-${var.environment_name}"

  tags = var.tags
}

resource "aws_secretsmanager_secret_rotation" "default" {
  count = var.use_password ? 1 : 0
  
  secret_id           = aws_secretsmanager_secret.default[0].id
  rotation_lambda_arn = aws_lambda_function.default[0].arn

  rotation_rules {
    automatically_after_days = var.password_rotate_after_days
  }
  
  tags = var.tags
}

resource "aws_secretsmanager_secret_version" "default" {
  count = var.use_password ? 1 : 0
  
  secret_id     = aws_secretsmanager_secret.default[0].id
  secret_string = jsonencode({"password"= local.random_password})
}