terraform {
  required_version = ">= 1.0.3"
  required_providers {
    aws = ">= 2.68"
  }
}


provider "aws" {
  region = var.region
}