locals {
  redis_constant = "redis"

  elasticache_service_name = "elasticache"

  lambda_name = "Secret-Rotate-${var.cluster_name}-${var.project_name}-${var.environment_name}"


  create_new_kms = var.rest_encryption_kms_id == "" && var.rest_encryption_enable

  random_password = var.use_password ? random_password.password.result : null

  automatic_failover_enabled = var.cluster_mode == null ? (var.automatic_failover_enabled && var.engine.number_cache_clusters > 1 ? true : false) : true
}

resource "random_password" "password" {
  length  = var.password_length
  special = false
}
