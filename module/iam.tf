resource "aws_iam_role_policy_attachment" "lambda_logs" {
  count = var.use_password ? 1 : 0

  role       = aws_iam_role.rotate_lambda[0].name
  policy_arn = aws_iam_policy.rotate_policy[0].arn
}

resource "aws_iam_role" "rotate_lambda" {
  count = var.use_password ? 1 : 0

  name = "Role-Lambda-Rotate-${var.cluster_name}-${var.environment_name}"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    sid = "AssumeRoleAccess"
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }

    actions = [
      "sts:AssumeRole",
    ]
  }
}

resource "aws_iam_policy" "rotate_policy" {
  count = var.use_password ? 1 : 0

  name        = "Lambda-Policy-${var.cluster_name}-${var.environment_name}"
  policy = data.aws_iam_policy_document.lambda_rotate_policy.json
}


data "aws_iam_policy_document" "lambda_rotate_policy" {
  statement {
    sid = "LogsAccess"
    effect = "Allow"
    
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]

    resources = [
       "arn:aws:logs:*:*:*",
    ]
  }

  statement {
    sid = "SecretManagerAccess"
    effect = "Allow"
    
    actions = [
      "secretsmanager:DescribeSecret",
      "secretsmanager:GetSecretValue",
      "secretsmanager:PutSecretValue",
      "secretsmanager:UpdateSecretVersionStage",
    ]

    resources = [
      aws_secretsmanager_secret.default[0].arn,
    ]
  }

  statement {
    sid = "AllowGenerateRandomPassword"
    effect = "Allow"
    
    actions = [
      "secretsmanager:GetRandomPassword",
    ]

    resources = [ "*" ]
  }

  statement {
    sid = "ElasticacheAccess"
    effect = "Allow"
    
    actions = [
      "elasticache:*",
    ]

    resources = [
      aws_elasticache_replication_group.default.arn,
    ]
  }
}