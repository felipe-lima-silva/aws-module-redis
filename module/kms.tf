data "aws_kms_key" "existent" {
  count  = local.create_new_kms ? 0 : 1

  key_id = var.rest_encryption_kms_id
}



resource "aws_kms_key" "new" {
  count  = local.create_new_kms ? 1 : 0
  tags = var.tags
}

resource "aws_kms_alias" "new" {
  count  = local.create_new_kms ? 1 : 0

  name          = "alias/${var.cluster_name}-${var.environment_name}"
  target_key_id = aws_kms_key.new[0].key_id
}