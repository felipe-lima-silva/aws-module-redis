resource "aws_elasticache_replication_group" "default" {
  replication_group_id          = "Cluster-${var.cluster_name}-${var.environment_name}"
  replication_group_description = "Elasticache Replication Group to ${var.cluster_name} in ${var.environment_name}"

  engine                = var.engine.type
  node_type             = var.engine.instance
  engine_version        = var.engine.version
  port                  = var.engine.port
 
  parameter_group_name  = aws_elasticache_parameter_group.default.name
  subnet_group_name     = aws_elasticache_subnet_group.default.name
  security_group_ids = [ aws_security_group.default.id ]
    
  apply_immediately     = var.apply_immediately
  maintenance_window    = var.maintenance_window
  snapshot_window       = local.redis_constant == var.engine.type ? var.snapshot_window : null

  number_cache_clusters =  var.cluster_mode == null ? null : var.engine.number_cache_clusters

  kms_key_id                 = var.rest_encryption_enable ? ( local.create_new_kms ?  aws_kms_key.new[0].id : data.aws_kms_key.existent[0].id ) : null
  auth_token                 = local.random_password
  transit_encryption_enabled = var.use_password ? true : var.transit_encryption_enabled

  automatic_failover_enabled = local.automatic_failover_enabled
  
  dynamic "cluster_mode" {
    for_each = var.cluster_mode == null ? [] : [1]
    content {
      replicas_per_node_group = var.cluster_mode.replicas_per_node_group
      num_node_groups         = var.cluster_mode.num_node_groups
    }
  }

  lifecycle {
    ignore_changes = [number_cache_clusters, kms_key_id]
  }

  tags = var.tags
}

resource "aws_elasticache_subnet_group" "default" {
  name       = "Subnets-${var.cluster_name}-${var.environment_name}"
  subnet_ids = var.subnet_ids
}

resource "aws_elasticache_parameter_group" "default" {
  name   = "Param-${var.cluster_name}-${var.environment_name}"
  family = var.parameter_group_family

  dynamic "parameter" {
    for_each = var.cluster_mode == null ? var.parameter_group_parameters : concat([{ name = "cluster-enabled", value = "yes" }], var.parameter_group_parameters)
    content {
      name  = parameter.value.name
      value = parameter.value.value
    }
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = var.tags
}
