terraform {
  backend "s3" {
    bucket         = "masf-brlink-dev-terraform-states-bucket" # it needs to be change
    key            = "infraestrutura/aws-backup.tfstate"
    region         = "eu-west-1"                                  # it needs to be change correct one it is ca-central-1
    dynamodb_table = "masf-brlink-dev-terraform-state-lock-table" # it needs to be change
    #profile        = "nome_do_projeto_dev"  # it needs to be change
  }
}


locals {
  region = "eu-west-1"
  vpc_id = "vpc-021f15add0fb928f7"
}

provider "aws" {
  region = local.region
}

data "aws_vpc" "selected" {
  id = local.vpc_id
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.selected.id
}


module "sample_elasticache" {
  source = "../module"

  region = local.region

  project_name     = "brlink"
  cluster_name     = "test"
  environment_name = "dev"

  engine = {
    type                  = "redis"
    version               = "6.x"
    instance              = "cache.t2.micro"
    number_cache_clusters = 1
    port                  = 6379
  }

  vpc_id = local.vpc_id

  subnet_ids             = ["subnet-07805bb2c4457f4df"] # data.aws_subnet_ids.all.ids
  parameter_group_family = "redis6.x"
}
