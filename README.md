# REDIS (ELASTICACHE)

### Name Formation

* Elasticache

    1. Security group:  sg-<cluster_name>-<environment>
    2. Subnet group:  subnets-<cluster_name>-<environment>
    3. Parameter group: params-<cluster_name>-<environment>

### Default configuration 

* Encryption in rest


### Variables

| Name                       | Type            | required | Description                                                                                                                                                                                                                                                               | Default                           |
| -------------------------- | --------------- | -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------- |
| project_name               | string          | [v]      | Name of the project.                                                                                                                                                                                                                                                      |                                   |
| region                     | string          | [v]      | This is the region where the resources will be applied                                                                                                                                                                                                                    |                                   |
| cluster_name               | string          | [v]      | This will be the name of the cluster                                                                                                                                                                                                                                      |                                   |
| environment_name           | string          | [v]      | The name of the environment. must be `dev`, `hml`, `prd`                                                                                                                                                                                                                  |                                   |
| engine                     | object          | [v]      | the engine configuration to be apply on cluster                                                                                                                                                                                                                           |                                   |
| cluster_mode               | object          | [v]      | Create a native Redis cluster                                                                                                                                                                                                                                             | {}                                |
| vpc_id                     | string          | [v]      |                                                                                                                                                                                                                                                                           |                                   |
| subnet_ids                 | list of  string | [v]      |                                                                                                                                                                                                                                                                           |                                   |
| parameter_group_family     | string          | []       | The family of the ElastiCache parameter group.                                                                                                                                                                                                                            | ''                                |
| parameter_group_parameters | list of map     | []       | The list of ElastiCache parameters to apply.                                                                                                                                                                                                                              | [] Ex. [  { name: "", value= ""}] |
| use_password               | bool            | []       | Define if the redis will create with Auth token                                                                                                                                                                                                                           | true                              |
| password_length            | number          | [v]      | Optional Unless `use_password` is `true`. Specifies the length of password                                                                                                                                                                                                | 32                                |
| password_rotate_after_days | number          | []       | Number of the day that the Auth token will roatate, will apply ony if password is true                                                                                                                                                                                    | 30                                |
| apply_immediately          | bool            | []       | Specifies whether any modifications are applied immediately, or during the next maintenance window                                                                                                                                                                        | true                              |
| maintenance_window         | string          | []       | Specifies the weekly time range for when maintenance on the cache cluster is performed                                                                                                                                                                                    | ''                                |
| snapshot_window            | string          | []       | The daily time range (in UTC) during which ElastiCache will begin taking a daily snapshot of your cache cluster.                                                                                                                                                          | ''                                |
| automatic_failover_enabled | bool            | []       | Specifies whether a read-only replica will be automatically promoted to read/write primary if the existing primary fails.                                                                                                                                                 | false                             |
| transit_encryption_enabled | bool            | []       | Whether to enable encryption in transit.                                                                                                                                                                                                                                  | true                              |
| rest_encryption_enable     | bool            | []       | Whether to enable encryption at rest.                                                                                                                                                                                                                                     | true                              |
| rest_encryption_kms_id     | string          | []       | The id of the KMS already existent if you desier that will be created keep empty, if filled the rest_encryption_enable must be true                                                                                                                                       | ''                                |
| rotate_lambda              | object          | [v]      | Lambda responsable to rotate the password.                                                                                                                                                                                                                                |
| log_retention              | number          | []       | Specifies the number of days you want to retain log events in the specified log group. Possible values are: 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653. If you select 0, the events in the log group are always retained and never expire. | 14                                |
| tags                       | map             | []       | A map of tags to assign to the resource                                                                                                                                                                                                                                   | {}                                |

##### Engine

| Name                  | Type   | required | Description                                                                                                                                                                                                                                                                                                                   | Default |
| --------------------- | ------ | -------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------- |
| type                  | string | [v]      | T) The name of the cache engine to be used for the clusters in this replication group. The only valid value is `redis`                                                                                                                                                                                                        |         |
| version               | string | [v]      | The version number of the cache engine to be used for the cache clusters in this replication group.                                                                                                                                                                                                                           |         |
| instance              | string | [v]      | The instance class to be used. See AWS documentation for information on [supported node types](https://docs.aws.amazon.com/AmazonElastiCache/latest/red-ug/CacheNodes.SupportedTypes.html) and [guidance on selecting node types](https://docs.aws.amazon.com/AmazonElastiCache/latest/red-ug/CacheNodes.SupportedTypes.html) |         |
| number_cache_clusters | number | [v]      | The number of cache clusters (primary and replicas)                                                                                                                                                                                                                                                                           |         |
| port                  | number | [v]      | the port number where the cluster will be setted (For Memcache the default is 11211, and for Redis the default port is 6379.)                                                                                                                                                                                                 |

### Cluster Mode

| Name                    | Type   | required | Description                                                          | Default |
| ----------------------- | ------ | -------- | -------------------------------------------------------------------- | ------- |
| replicas_per_node_group | number | [v]      | Number of replica nodes in each node group. Valid values are 0 to 5. |         |
| num_node_groups         | number | [v]      | umber of node groups (shards) for this Redis replication group.      |         |

##### rotate_lambda

| Name    | Type   | required | Description                            | Default           |
| ------- | ------ | -------- | -------------------------------------- | ----------------- |
| zip     | string | [v]      | File that will be import to Aws Lambda | lambda_rotate.zip |
| handler | string | [v]      | The Lambda entrypoint                  | main              |